#include "DifferentialEvolution.hpp"
#include "TimeHelpers.hpp"
#include <iostream>

using Point = std::vector<double>;
int pop_size = 40;
int d = 10;
int num_epochs = 300;
Random R;

Point random_point()
{
    Point P(d);
    std::generate(P.begin(), P.end(), []() { return R.random_real(100, 201); });
    return P;
}

int main()
{
    Chronometer C;
    std::vector<Point> Population(pop_size);
    std::generate(Population.begin(), Population.end(), []() {
        return random_point();
    });

    DifferentialEvolver D(Population, [](const Point& p) {
        double result = 0.0;
        for (auto s : p)
            result += s*s;
        return result;
    });

    double best = D.best_cost;

    for (int epoch : NN(num_epochs))
    {
        D.step(0.5, 0.5);
        if (D.best_cost < best)
        {
            best = D.best_cost;
            std::cout << epoch << "  cost: " << best << std::endl;
        }
    }
    std::cout << "Total time taken: " << C.Peek() << "s\n";
    return 0;
}