#include "DifferentialEvolution.hpp"
#include "Random.hpp"
#include <gtest/gtest.h>
#include <iostream>

TEST(HelloWorld, FirstTest) { ASSERT_TRUE(true); }

TEST(DiffEvo, EasyCase)
{
    using Point = std::vector<double>;
    int pop_size = 40;
    int d = 3;
    int num_epochs = 300;
    Random R;

    auto random_point = [&R, d]() {
        Point P(d);
        std::generate(P.begin(), P.end(), [&R]() {
            return R.random_real(100, 201);
        });
        return P;
    };

    std::vector<Point> Population(pop_size);
    std::generate(Population.begin(), Population.end(), [random_point]() {
        return random_point();
    });

    DifferentialEvolver D(Population, [](const Point& p) {
        double result = 0.0;
        for (auto s : p)
            result += s*s;
        return result;
    });

    double best = D.best_cost;

    for (int epoch : NN(num_epochs))
    {
        D.step(0.5, 0.5);
        if (D.best_cost < best)
        {
            best = D.best_cost;
        }
    }
    ASSERT_NEAR(best, 0.0, 0.00000001);
}
